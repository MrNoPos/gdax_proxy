Rails.application.routes.draw do
  post 'deal/make' => 'deals#make', defaults: { format: 'json' }
end
